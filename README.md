Eureka 
=======

Proyecto de ejemplo de microservicio spring-boot que consta del levantamiento de un servidor Eureka de Netflix
para regitrar microservicios.

Es una prueba de concepto para entender el funcionamiento de estas herramientas.


¿Qué es EUREKA?
=========
Eureka es un servidor para el registro y localización de microservicios, 
balanceo de carga y tolerancia a fallos. La función de Eureka es registrar 
las diferentes instancias de microservicios existentes, su localización, estado, metadatos…

¿Cómo funciona?
==================
Cada microservicio, durante su arranque, se comunicará con el servidor Eureka 
para notificar que está disponible, dónde está situado, sus metadatos… 
De esta forma Eureka mantendrá en su registro la información de todos los microservicios del ecosistema.
El nuevo microservicio continuará notificando a Eureka su estado cada 30 segundos,
lo que denominan ‘heartbeats’. Si después de tres periodos Eureka no recibe notificación
de dicho microservicio lo eliminará del registro. De la misma forma una vez vueltos 
a recibir tres notificaciones considerará el servicio disponible de nuevo.

Cada cliente de Eureka podrá también recuperar el registro para localizar otros 
microservicios con los que necesite comunicarse. Dicha información de registro se 
cachea en el cliente. Eureka se puede configurar para funcionar en modo cluster donde 
varias instancias “peer” intercambiarán su información. Esto, junto al cacheo de la 
información de registro en los clientes da a Eureka una alta tolerancia a fallos.



ref : https://www.paradigmadigital.com/dev/quien-es-quien-en-la-arquitectura-de-microservicios-spring-cloud-12/